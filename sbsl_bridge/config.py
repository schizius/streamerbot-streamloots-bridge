import configparser
import pathlib
import platformdirs

def _config_path():
    dirname = platformdirs.user_config_dir("SBSLBridge", ensure_exists=True)
    cfgpath = pathlib.Path(dirname).joinpath('SBSLBridge.ini')
    return str(cfgpath)

CONFIG_PATH = _config_path()

_cfg = None

def load():
    global _cfg
    _cfg = configparser.ConfigParser()

    try:
        with open(CONFIG_PATH, 'r') as cfg_file:
            _cfg.read_file(cfg_file)
    except FileNotFoundError:
        _cfg['SBSL'] = { "AutoStart": "False" }
        _cfg['StreamLoots'] = { "UUID": "00000000-0000-0000-0000-000000000000" }
        _cfg['StreamerBot'] = { "URI": "http://127.0.0.1:7474" }

def store():
    with open(CONFIG_PATH, 'w') as cfg_file:
        _cfg.write(cfg_file)

def _get(section, key, default=None):
    global _cfg
    return _cfg.get(section, key, fallback=default)

def _getbool(section, key, default=False):
    global _cfg
    return _cfg.getboolean(section, key, fallback=default)

def _set(section, key, value):
    global _cfg

    try:
        _cfg.add_section(section)
    except configparser.DuplicateSectionError:
        pass

    _cfg[section][key] = value

def get_uuid():
    return _get("StreamLoots","UUID")

def set_uuid(uuid):
    _set("StreamLoots", "UUID", uuid)

def get_uri():
    return _get("StreamerBot","URI")

def set_uri(uri):
    _set("StreamerBot","URI", uri)

def get_autostart():
    return _getbool("SBSL", "AutoStart", False)

def set_autostart(autostart):
    _set("SBSL", "AutoStart", str(autostart))
