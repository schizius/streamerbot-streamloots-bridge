import requests

class ActionSink():
    def __init__(self, uri):
        self.uri = uri

    def __enter__(self):
        return self

    def __exit__(self, *args):
        pass

    def do_action(self, name, args):
        req = requests.post(self.uri + '/DoAction', json={
            "action": { "name": name },
            "args": args,
        })

        req.raise_for_status()

    def test(self):
        try:
            req = requests.get(self.uri + '/TestCredits')
            req.raise_for_status()
            return not req.json() is None
        except:
            return False
