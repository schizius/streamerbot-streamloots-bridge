from tkinter import Tk, ttk, messagebox, BooleanVar
from functools import partialmethod

version = "v0.4"

class GUI():
    def __init__(self, uuid, uri, autostart):
        self.on_start = None
        self.on_quit = None
        self.confirm_quit = False

        self.root = Tk()
        self.root.title(f"SBSL Bridge {version}")
        self.root.resizable(0,0)
        self.root.wm_protocol("WM_DELETE_WINDOW", self.__on_quit)

        self.frame = ttk.Frame(self.root, padding=10)
        self.frame.grid()

        label = ttk.Label(self.frame, text="StreamLoots UUID  ", anchor="e", width=20)
        label.grid(column=0, row=0)
        entry = ttk.Entry(self.frame, width=40)
        entry.grid(column=1, row=0, columnspan=2)
        entry.insert(0, uuid)
        self.uuid = entry

        label = ttk.Label(self.frame, text="StreamerBot URI  ", anchor="e", width=20)
        label.grid(column=0, row=1)
        entry = ttk.Entry(self.frame, width=40)
        entry.grid(column=1, row=1, columnspan=2)
        entry.insert(0, uri)
        self.uri = entry

        button = ttk.Button(self.frame, text="Start", command=self.__on_start)
        button.grid(column=0, row=2)
        self.start = button

        self.autostart = BooleanVar(value=autostart)
        check = ttk.Checkbutton(self.frame, text="Autostart", var=self.autostart,
                                onvalue=True, offvalue=False,
                                command=self.__on_toggle_autostart)
        check.grid(column=2, row=2)

        ttk.Button(self.frame, text="Quit", command=self.__on_quit).grid(column=1, row=2)

        ttk.Label(self.frame, text="").grid(column=0, row=3, columnspan=3)
        ttk.Label(self.frame, text=f"Streamer.bot StreamLoots Bridge {version} by Schizius").grid(column=0, row=4, columnspan=3)

    def run(self):
        if self.autostart.get():
            self._on_start()
        self.root.mainloop()

    def set_start_text(self, text):
        self.start.config(text=text)

    def set_confirm_quit(self, confirm):
        self.confirm_quit = confirm

    def _on_start(self):
        if self.on_start:
            self.on_start(self.uuid.get(), self.uri.get(), self.autostart.get())

    def _on_quit(self):
        if self.confirm_quit:
            answer = messagebox.askyesno("Confirm","Bridge is running. Stop and exit?")

            if not answer:
                return

        if self.on_quit:
            self.on_quit()
        self.root.destroy()

    def _on_toggle_autostart(self):
        if self.on_toggle_autostart:
            self.on_toggle_autostart(self.autostart.get())

    __on_start = partialmethod(_on_start)
    __on_quit = partialmethod(_on_quit)
    __on_toggle_autostart = partialmethod(_on_toggle_autostart)

def error(msg):
    messagebox.showerror(message=msg)
