import json
import requests

class BaseStream():
    def __init__(self, endpoint, uuid):
        self.uri = f'https://widgets.streamloots.com/{endpoint}/{uuid}/media-stream'

    def connect(self):
        self.stream = requests.get(self.uri, stream=True)

    def close(self):
        self.stream.close()
        del self.stream

    def __enter__(self, *_):
        self.connect()
        return self

    def __exit__(self, *_):
        self.close()

    def iter(self):
        for line in self.stream.iter_lines():
            line = line.decode('UTF-8')

            if not line.startswith('data: '):
                continue

            data = json.loads(line[6:])

            try:
                if data['type'] != 'alert':
                    continue
            except KeyError:
                continue

            yield data

class AlertsStream(BaseStream):
    def __init__(self, uuid):
        super().__init__('alerts', uuid)
