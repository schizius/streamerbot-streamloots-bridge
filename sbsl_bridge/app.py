from . import config, gui
from . import streamerbot
from . import streamloots

import multiprocessing
import sys
import traceback

class MockSink():
    def do_action(self, **kwargs):
        print(kwargs)

def iter_fields(l):
    for f in l:
        try:
            name = f['name']
            value = f['value']
        except KeyError:
            continue

        yield name, value

def kvlist_to_dict(parent, key):
    l = parent.get(key, list())
    return { k:v for k,v in iter_fields(l) }

def redemption_to_action(alert):
    data = alert['data']

    fields = kvlist_to_dict(data, 'fields')

    return {
        'name': f'StreamLoots: {data["cardName"]}',
        'args': {
            'cardMessage': alert['message'],
            'cardDuration': data['duration'],
            'cardUserName': fields.get('username', 'unknown'),
            'cardUserMessage': fields.get('message',''),
            'cardRarity': data['cardRarity'],
        },
    }

def purchase_to_action(alert):
    data = alert['data']

    fields = kvlist_to_dict(data, 'fields')

    try:
        quantity = fields['quantity']
    except KeyError:
        return None

    return {
        'name': 'StreamLoots Purchase',
        'args': {
            'purchaseUserName': fields.get('username', 'unknown'),
            'purchaseQuantity': quantity,
        }
    }

def alert_to_action(alert):
    try:
        alert_type = alert['data']['type']
    except KeyError:
        return None

    if alert_type == 'redemption':
        return redemption_to_action(alert)
    elif alert_type == 'purchase' or alert_type == 'random-community-gift':
        return purchase_to_action(alert)

    return None

def handle_alert(alert, sink):
    try:
        action = alert_to_action(alert)
    except:
        print("Failed to build action from alert:\n", alert, file=sys.stderr)
        traceback.print_exc()
        return

    if action is None:
        return

    try:
        sink.do_action(**action)
    except:
        print("Failed to post action:\n", action, file=sys.stderr)
        traceback.print_exc()
        return

def run_bridge(uuid, uri):
    sink = streamerbot.ActionSink(uri)

    with streamloots.AlertsStream(uuid) as alerts:
        for alert in alerts.iter():
            handle_alert(alert, sink)
            del alert

def _run_bridge(uuid, uri):
    while True:
        try:
            run_bridge(uuid, uri)
        except KeyboardInterrupt:
            return
        except:
            print("Exception escaped, restarting", file=sys.stderr)
            traceback.print_exc()

_gui = None
_process = None

def _update_start_button():
    global _gui
    global _process

    if not _process is None:
        _gui.on_start = on_stop
        _gui.set_start_text("Stop")
        _gui.confirm_quit = True
    else:
        _gui.on_start = on_start
        _gui.set_start_text("Start")
        _gui.confirm_quit = False

def on_toggle_autostart(value):
    config.set_autostart(value)
    config.store()

def on_start(uuid, uri, autostart):
    global _process
    on_quit()

    sink = streamerbot.ActionSink(uri)

    if not sink.test():
        gui.error("StreamerBot Test Connection Failed")
        return

    config.set_uuid(uuid)
    config.set_uri(uri)
    config.set_autostart(autostart)
    config.store()

    _process = multiprocessing.Process(target=_run_bridge, args=(uuid,uri))
    _process.start()
    _update_start_button()

def on_stop(*args):
    on_quit()

def on_quit():
    global _process
    global _gui

    if not _process is None:
        _process.terminate()
        _process.join()
        _process = None

    _update_start_button()

def run():
    global _gui
    config.load()

    _gui = gui.GUI(config.get_uuid(), config.get_uri(), config.get_autostart())
    _gui.on_start = on_start
    _gui.on_quit = on_quit
    _gui.on_toggle_autostart = on_toggle_autostart
    _gui.run()

    return 0
