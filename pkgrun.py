from sbsl_bridge import app
import multiprocessing
import sys

if __name__ == "__main__":
    multiprocessing.freeze_support()
    sys.exit(app.run())
